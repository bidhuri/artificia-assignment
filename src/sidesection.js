import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import All from '@material-ui/icons/AllInbox';
import View from '@material-ui/icons/ViewModule';
import In from '@material-ui/icons/InsertChart';
import MenuList from '@material-ui/core/MenuList';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

const styles = theme => ({
    menuItem: {
        shadow: "10px black",
        margin:"20px",
      '&:focus': {
        backgroundColor: theme.palette.primary.main,
        '& $primary, & $icon': {
          color: theme.palette.common.white,
        },
      },
    },
    primary: {},
    icon: {},
  });

function ListItemComposition(props) {
    const { classes } = props;
  
    return (
        <MenuList>
          <MenuItem className={classes.menuItem}>
            <ListItemIcon className={classes.icon}>
              <All/>
            </ListItemIcon>
            <ListItemText classes={{ primary: classes.primary }} inset primary="Library" />
          </MenuItem>
          <MenuItem className={classes.menuItem}>
            <ListItemIcon className={classes.icon}>
              <View/>
            </ListItemIcon>
            <ListItemText classes={{ primary: classes.primary }} inset primary="Gallery" />
          </MenuItem>
          <MenuItem className={classes.menuItem}>
            <ListItemIcon className={classes.icon}>
              <In/>
            </ListItemIcon>
            <ListItemText classes={{ primary: classes.primary }} inset primary="Analytics" />
          </MenuItem>
        </MenuList>
    );
  }
  
  ListItemComposition.propTypes = {
    classes: PropTypes.object.isRequired,
  };
  
  export default withStyles(styles)(ListItemComposition);
  