import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
const styles = {
  card: {
    maxWidth: 250,
    margin:"30px"
  },
  media: {
    // ⚠️ object-fit is not supported by IE 11.
    objectFit: 'cover',
  },title: {
    width:"100%",
  }
};
const center={
    marginLeft:"15%"
};
function ImgMediaCard(props) {
  const { classes } = props;
  return (
    <Card className={classes.card}>
      <CardActionArea>
        <CardMedia
          
          component="img"
          alt="Contemplative Reptile"
          className={classes.media}
          height="150"
          image={props.im}
          title="Contemplative Reptile"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" color="primary" component="h2">
            {props.author}
          </Typography>
          <Typography gutterBottom  component="p">
            {props.date}
          </Typography>
          <Typography component="p">
          {props.desc}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions style={center}>
        <Button variant="outlined" size="large" color="primary">
        <Typography className={classes.title} color="primary" component="h6">Tag Products</Typography>
        </Button>
      </CardActions>
    </Card>
  );
}

ImgMediaCard.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ImgMediaCard);