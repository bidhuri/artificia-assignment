import React, { Component } from 'react';
import Side from './sidesection';
import {  Row } from 'simple-flexbox';
import Toolbar from './toolbar';
import Button from '@material-ui/core/Button';
import green from '@material-ui/core/colors/green';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Down from '@material-ui/icons/KeyboardArrowDown';
import Card from './cards';
import Grid from '@material-ui/core/Grid';
import GridList from "@material-ui/core/GridList";
const theme = createMuiTheme({
  palette: {
    secondary: green,
    // Used by `getContrastText()` to maximize the contrast between the background and
    // the text.
    contrastThreshold: 3,
    // Used to shift a color's luminance by approximately
    // two indexes within its tonal palette.
    // E.g., shift from Red 500 to Red 300 or Red 700.
    tonalOffset: 0.2,
  },
});
const side={
  position: "fixed"
};
const top={
  marginTop:"1%"
};
class App extends Component {
  state = {
    anchorEl: null,
  };

  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };
  render() {
    const { anchorEl } = this.state;
    var Name="Akhil Bidhuri";//Hard Coded can be as fetected from DB(User Data)
    var products = [{id:1,description:"This is fashion based tagging of content on the internet to drive the slaes of the brands.",time:Date().slice(0, 24),url:"https://i.pinimg.com/originals/8f/d6/04/8fd60438ab08c168bf3cd3cc008a5b45.gif"}
                    ,{id:2,description:"This is fashion based tagging of content on the internet to drive the slaes of the brands.",time:Date().slice(0, 24),url:"https://i.pinimg.com/originals/8f/d6/04/8fd60438ab08c168bf3cd3cc008a5b45.gif"}
                    ,{id:3,description:"This is fashion based tagging of content on the internet to drive the slaes of the brands.",time:Date().slice(0, 24),url:"https://i.pinimg.com/originals/8f/d6/04/8fd60438ab08c168bf3cd3cc008a5b45.gif"}
                    ,{id:4,description:"This is fashion based tagging of content on the internet to drive the slaes of the brands.",time:Date().slice(0, 24),url:"https://i.pinimg.com/originals/8f/d6/04/8fd60438ab08c168bf3cd3cc008a5b45.gif"}
                    ,{id:5,description:"This is fashion based tagging of content on the internet to drive the slaes of the brands.",time:Date().slice(0, 24),url:"https://i.pinimg.com/originals/8f/d6/04/8fd60438ab08c168bf3cd3cc008a5b45.gif"}
                    ,{id:6,description:"This is fashion based tagging of content on the internet to drive the slaes of the brands.",time:Date().slice(0, 24),url:"https://i.pinimg.com/originals/8f/d6/04/8fd60438ab08c168bf3cd3cc008a5b45.gif"}
                    ,{id:7,description:"This is fashion based tagging of content on the internet to drive the slaes of the brands.",time:Date().slice(0, 24),url:"https://i.pinimg.com/originals/8f/d6/04/8fd60438ab08c168bf3cd3cc008a5b45.gif"}]
    return (
      <MuiThemeProvider theme={theme}>
      <div className="App">
      <Toolbar Name={Name}/>
      <Grid container spacing={24}>
      <Grid item xs={3} style={side}>
        <Side/>
        </Grid><Grid item xs={3}></Grid>
        <Grid item xs={9} style={top}>
        <Row>
        <Button  variant="outlined" size="large" color="secondary" >
        Refresh
      </Button>
      <Grid item xs={6}></Grid>
      <Button size="large" aria-owns={anchorEl ? 'simple-menu' : undefined}
          aria-haspopup="true"
          onClick={this.handleClick}  variant="outlined" >
        All Photos
        <Down/>
      </Button>
      <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={this.handleClose}
        >
          <MenuItem onClick={this.handleClose}>Some Photos</MenuItem>
          <MenuItem onClick={this.handleClose}>Some Photos</MenuItem>
          <MenuItem onClick={this.handleClose}>Some Photos</MenuItem>
        </Menu></Row>
        <GridList cols={3}>

          {products.map(e => (<Card
                        id = {e.id}
                        author = {Name}
                        desc = {e.description}
                        im = {e.url}
                        date = {e.time}/>
                    ))}</GridList></Grid>
        </Grid>        
      </div>
      </MuiThemeProvider>
    );
  }
}

export default App;
